# Testing with Vagrant

## Preparing VM

Vagrant creates VMs with 10 GB virtual disk. This is not enough for a WoW server. And you can't make Vagrant create bigger disks, what a pity. So, you have two options: resize the disk or add a second one.

In either case, think twice before `vagrant destroy`, because you have to do the same work again! :-)

### The hard way: resize disk.

Since `VBoxManage` can not resize `vmdk` images, it has to be replaced by a `vdi` image. Locate the Vagrant VM and:

```
$ VBoxManage clonehd "source.vmdk" "cloned.vdi" --format vdi
$ VBoxManage modifyhd "cloned.vdi" --resize $((24*1024))
```

Open VM in VirtualBox Manager, replace original `vmdk` disk with resized `vdi` image.

The virtual disk has been resized, now the partitions and filesystem has to be expanded. After booting up the VM:

```
$ vagrant ssh
$ sudo swapoff /dev/sda5
$ sudo fdisk /dev/sda
```

Because of the partition layout, it is not possible to resize the root partition in one step. The swap partition has to be removed and rebuilt later:

* Remove partition 5 (swap space)
* Remove partition 2 (extended partition)
* Also remove partition 1 (root partition)
* Create new primary partition 1 starting with same sector as before (2048), last sector = total_blocks - swap_space_blocks = 48238591
* Create new primary partition 2 with type 82 (swap)
* Write partition table and exit

Now:

* Remove swap space from `/etc/fstab`
* `sudo reboot`
* `vagrant ssh` again
* `sudo mkswap /dev/sda2`
* `sudo swapon /dev/sda2`
* Add new swap space to `/etc/fstab`
* `sudo resize2fs /dev/sda1`

### The easier way: add a second disk.

Open VirtualBox Manager, add a second disk, size 16 GB or more. Start up the VM, format the disk `/dev/sdb` with `ext4` and mount it to `/srv`. That's all, you are done.
