# CMaNGOS classic server

Purpose of this project is the automated deployment of a working [CMaNGOS-classic][1] server using [Ansible](https://www.ansible.com/). It does work - at least for me, your mileage may vary. To be honest: it was my intention to learn Ansible :-).

## Requirements

First of all: You have to install your purchased WoW game client. For [CMaNGOS-classic][1] you need the very first 'World of Warcraft' release, patch version 1.12. A wine install is ok, WoW client runs fine with wine. If you lost your WoW setup CD, you may have success with an internet search to locate the client.

The target PC or VM has to meet or exceed those requirements:

* 16 GB free hard disk space
* 2 GB of RAM
* 2 CPU cores
* Debian 9 (stretch)
* SSH service running

## Installation

Edit `wow-server.yml` and adapt it to your environment:

* Point ansible variable `wowserver_client_data` to the `Data` directory of your WoW installation. If you installed the client into the default Wine prefix, it would be `~/.wine/drive_c/Program Files (x86)/World of Warcraft/Data`
* Optional: Point ansible variable `wowserver_client_installer` to the WoW client setup program or archive. Example: `~/MyGames/World_of_Warcraft_1.12_Client.zip`. When defined this file will be copied to a samba share for easy access.

## Further reading

* More about [CMaNGOS project][2] with detailed [installation instructions][3]
* [CMaNGOS Github repository][1]
* [Classic content database Github repository][4]

[1]: https://github.com/cmangos/mangos-classic
[2]: http://cmangos.net/
[3]: https://github.com/cmangos/issues/wiki/Installation-Instructions
[4]: https://github.com/cmangos/classic-db
